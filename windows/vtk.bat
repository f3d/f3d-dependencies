git clone https://gitlab.kitware.com/vtk/vtk.git
cd vtk
git checkout 37fe47d9
git submodule update --init --recursive
cd ../
mkdir vtk_build
cd vtk_build
cmake -Ax64 -DCMAKE_POLICY_DEFAULT_CMP0091=NEW -DCMAKE_MSVC_RUNTIME_LIBRARY=MultiThreaded ^
  -DVTK_ENABLE_LOGGING=OFF                                                                ^
  -DVTK_BUILD_TESTING=OFF                                                                 ^
  -DCMAKE_BUILD_TYPE=Release                                                              ^
  -DVTK_LEGACY_REMOVE=ON                                                                  ^
  -DBUILD_SHARED_LIBS=OFF                                                                 ^
  -DCMAKE_INSTALL_PREFIX:FILEPATH=../vtk_install                                          ^
  -DVTK_GROUP_ENABLE_Rendering=DEFAULT                                                    ^
  -DVTK_GROUP_ENABLE_StandAlone=DEFAULT                                                   ^
  -DVTK_MODULE_ENABLE_VTK_CommonSystem=YES                                                ^
  -DVTK_MODULE_ENABLE_VTK_FiltersGeneral=YES                                              ^
  -DVTK_MODULE_ENABLE_VTK_FiltersGeometry=YES                                             ^
  -DVTK_MODULE_ENABLE_VTK_ImagingCore=YES                                                 ^
  -DVTK_MODULE_ENABLE_VTK_ImagingHybrid=YES                                               ^
  -DVTK_MODULE_ENABLE_VTK_InteractionStyle=YES                                            ^
  -DVTK_MODULE_ENABLE_VTK_InteractionWidgets=YES                                          ^
  -DVTK_MODULE_ENABLE_VTK_IOCityGML=YES                                                   ^
  -DVTK_MODULE_ENABLE_VTK_IOExodus=YES                                                    ^
  -DVTK_MODULE_ENABLE_VTK_IOGeometry=YES                                                  ^
  -DVTK_MODULE_ENABLE_VTK_IOImage=YES                                                     ^
  -DVTK_MODULE_ENABLE_VTK_IOImport=YES                                                    ^
  -DVTK_MODULE_ENABLE_VTK_IOParallel=YES                                                  ^
  -DVTK_MODULE_ENABLE_VTK_IOPLY=YES                                                       ^
  -DVTK_MODULE_ENABLE_VTK_IOXML=YES                                                       ^
  -DVTK_MODULE_ENABLE_VTK_RenderingAnnotation=YES                                         ^
  -DVTK_MODULE_ENABLE_VTK_RenderingCore=YES                                               ^
  -DVTK_MODULE_ENABLE_VTK_RenderingLabel=YES                                              ^
  -DVTK_MODULE_ENABLE_VTK_RenderingOpenGL2=YES                                            ^
  -DVTK_MODULE_ENABLE_VTK_RenderingVolumeOpenGL2=YES                                      ^
  -DVTK_MODULE_ENABLE_VTK_jsoncpp=YES                                                     ^
  ../vtk
cmake --build . --config Release --parallel 4
cmake --build . --config Release --target install
cd ../
tar.exe -a -c -f vtk_install.zip vtk_install
