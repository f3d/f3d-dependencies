git clone https://git.dev.opencascade.org/repos/occt.git
cd occt
git checkout V7_5_1
git submodule update --init --recursive
cd ../
mkdir occt_build
cd occt_build
cmake -Ax64 -DCMAKE_POLICY_DEFAULT_CMP0091=NEW       ^
  -DCMAKE_MSVC_RUNTIME_LIBRARY=MultiThreaded         ^
  -DBUILD_LIBRARY_TYPE=Static                        ^
  -DBUILD_ADDITIONAL_TOOLKITS="TKSTEP;TKIGES;TKMesh" ^
  -DBUILD_MODULE_ApplicationFramework=OFF            ^
  -DBUILD_MODULE_DataExchange=OFF                    ^
  -DBUILD_MODULE_Draw=OFF                            ^
  -DBUILD_MODULE_FoundationClasses=OFF               ^
  -DBUILD_MODULE_ModelingAlgorithms=OFF              ^
  -DBUILD_MODULE_ModelingData=OFF                    ^
  -DBUILD_MODULE_Visualization=OFF                   ^
  -DCMAKE_BUILD_TYPE=Release                         ^
  -DINSTALL_DIR=../occt_install                       ^
  ../occt
cmake --build . --config Release --parallel 4
cmake --build . --config Release --target install
cd ../
tar.exe -a -c -f occt_install.zip occt_install
