# F3D Dependencies

A repository used by F3D CI in order to download and extract dependencies.

To update the VTK version in use in the CI of F3D, read the following.

## Linux

In the `linux` dir, edit the Dockerfile file and change the VTK or OpenCASCADE hash to checkout, then run

```
cd path/to/linux
docker login gitlab.kitware.com:4567
docker build -t gitlab.kitware.com:4567/f3d/f3d/linux-deps:latest .
docker build -t gitlab.kitware.com:4567/f3d/f3d/linux-deps:<hash> .
docker push gitlab.kitware.com:4567/f3d/f3d/linux-deps:latest
docker push gitlab.kitware.com:4567/f3d/f3d/linux-deps:<hash>
```

To test locally using a docker image:

```
Xephyr -br -ac -noreset -screen 1920x1080 :1
DISPLAY=:1 xterm
```

```
docker run -it -e DISPLAY=:1 -v="/tmp/.X11-unix:/tmp/.X11-unix:rw" --privileged ImageId /bin/bash
```

Then clone, configure, build and test f3d within the docker interactive shell.

## Windows

In the `windows` dir, edit the `vtk.bat` and change the hash to checkout, open `Visual Studio x64 Native command`, then run

```
cd C:\path\to\windows
vtk.bat
```

Open `git-bash`

```
cd C:/path/to/windows
cp vtk_install.zip ./archive/vtk-<hash>.zip
cp vtk_install.zip ./archive/vtk-latest.zip
git add archive/*
git commit
git push
```

If necessary, OpenCASCADE can be updated using the same method with the script `occt.bat`.

## macOS

In the `macos` dir, edit `vtk.sh` and change the hash to checkout, then run

```
cd path/to/macos
./vtk.sh
cp vtk_install.tar.gz ./archive/vtk-<hash>.tar.gz
cp vtk_install.tar.gz ./archive/vtk-latest.tar.gz
git add archive/*
git commit
git push
```

If necessary, OpenCASCADE can be updated using the same method with the script `occt.sh`.
