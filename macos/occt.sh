#!/bin/sh
export DEVELOPER_DIR="/Applications/Xcode-11.7.app/Contents/Developer"
git clone https://git.dev.opencascade.org/repos/occt.git
cd occt
git checkout V7_5_1
git submodule update --init --recursive
cd ../
mkdir occt_build
cd occt_build
cmake \
  -DBUILD_LIBRARY_TYPE=Static \
  -DBUILD_ADDITIONAL_TOOLKITS="TKSTEP;TKIGES;TKMesh" \
  -DBUILD_MODULE_ApplicationFramework=OFF \
  -DBUILD_MODULE_DataExchange=OFF \
  -DBUILD_MODULE_Draw=OFF \
  -DBUILD_MODULE_FoundationClasses=OFF \
  -DBUILD_MODULE_ModelingAlgorithms=OFF \
  -DBUILD_MODULE_ModelingData=OFF \
  -DBUILD_MODULE_Visualization=OFF \
  -DCMAKE_BUILD_TYPE=Release \
  -DINSTALL_DIR=../occt_install \
  ../occt
cmake --build . --config Release --parallel 4
cmake --build . --config Release --target install
cd ../
sed -i 's/OpenCASCADE_LIBRARIES [A-Za-z0-9;]\+/OpenCASCADE_LIBRARIES TKSTEP;TKSTEPAttr;TKSTEP209;TKSTEPBase;TKIGES;TKBool;TKMesh;TKXSBase;TKBO;TKPrim;TKShHealing;TKTopAlgo;TKBRep;TKGeomAlgo;TKGeomBase;TKG3d;TKG2d;TKMath;TKernel/' occt_install/lib/cmake/opencascade/OpenCASCADEConfig.cmake
tar -zcvf occt_install.tar.gz occt_install
